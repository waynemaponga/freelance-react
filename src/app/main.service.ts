import { Injectable } from '@angular/core';
import { User } from './home/user';
import * as io from 'socket.io-client';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';


@Injectable({
  providedIn: 'root'
})
export class MainService {
  socket: any;
  constructor( public toastController: ToastController , private router: Router , private jwtHelper: JwtHelper) {
  }

  Register(user: User) {
    this.socket = io('http://localhost:4000');
    this.Receive();
    this.socket.emit('registerUser', user);
  }
  Receive() {
    this.socket.on('message', (msg) => {

      if (msg.code ==  422) {
        this.showToast(msg.description);

      }

      if (msg.code ==  401) {
        this.showToast(msg.description);

      }
      if (msg.code == 201) {
        this.showToast(msg.description);
        this.router.navigateByUrl('/wc');
      }
      if (msg.code == 200) {
          var token =  this.jwtHelper.decodeToken(msg.token);
        this.showToast('Welcome !! ' + token.user.firstname);
        this.router.navigateByUrl('/wc');
      }

    });
 }
 showToast(messagecode: string) {
  this.toastController.create({
    message: messagecode,
    duration: 2000,
    animated: true,
    showCloseButton: true,
    closeButtonText: 'OK',
    position: 'middle'
  }).then((obj) => {
    obj.present();
  });
}

login(user: User) {
  this.socket = io('http://localhost:4000');
  this.socket.emit('login', user);
  this.Receive();
}

closeSocket() {
  this.socket.disconnect();
}
}
