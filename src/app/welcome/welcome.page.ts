import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(private router: Router , public  mainService: MainService, ) { }

  ngOnInit() {
  }
  logOut() {
   this.router.navigateByUrl('home');
  }
}
