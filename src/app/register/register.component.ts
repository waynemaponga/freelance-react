import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { User } from '../home/user';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public  user: User = {firstname: '', lastname: '', email: '', password: '' , mobileNumber: null};
  Errorcounter = 0;
  errorMessage = '';
  constructor(public  mainService: MainService,   public toastController: ToastController , private router: Router ) { }

  ngOnInit() {}


  login() {
    this.router.navigateByUrl('/home');
  }

  registerUser() {
    this.Errorcounter = 0;
    if (this.user.email  === '') {
    this.Errorcounter++;
  }

    if (this.user.firstname  === '')  {

     this.Errorcounter++;
    }

    if (this.user.lastname  === '') {
      this.Errorcounter++;
      }
    if (this.user.mobileNumber === null) {
        this.Errorcounter++;
        }
    if (this.user.mobileNumber.length < 10) {
         console.log('hhyy');
         this.Errorcounter++;
          }

    if  (this.user.password  === '') {
        this.Errorcounter++;
        }

    if (this.Errorcounter === 0) {
           this.mainService.Register(this.user);
         } else {
           this.errorMessage = 'Please enter  all details in  order to Register';
           this.showToast (this.errorMessage);
         }

  }
  showToast(messagecode: string) {
    this.toastController.create({
      message: messagecode,
      duration: 2000,
      animated: true,
      showCloseButton: true,
      closeButtonText: 'OK',
      position: 'middle'
    }).then((obj) => {
      obj.present();
    });
  }



}
