import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MainService } from './main.service';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { WelcomePageModule } from './welcome/welcome.module';
import { HomePage } from './home/home.page';
import { JwtHelper } from 'angular2-jwt';



@NgModule({
  declarations: [AppComponent , RegisterComponent , HomePage],
  entryComponents: [],
  imports: [BrowserModule , WelcomePageModule, FormsModule,  IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    JwtHelper,
    MainService,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
