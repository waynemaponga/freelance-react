import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main.service';
import { User } from './user';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  socket: any;
  Errorcounter = 0;
  public  user: User = {firstname: '', lastname: '', email: '', password: '' , mobileNumber: null};
  errorMessage = '';
  constructor(private router: Router, public  mainService: MainService , public toastController: ToastController) {

  }
  register() {
    this.router.navigateByUrl('/reg');
  }

  login() {
    this.Errorcounter = 0;
    if (this.user.email  === '') {
    this.Errorcounter++;
  }
    if  (this.user.password  === '') {
        this.Errorcounter++;
        }

    if (this.Errorcounter === 0) {
           this.mainService.login(this.user);
         } else {
           this.errorMessage = 'Please enter  your email or password';
           this.showToast (this.errorMessage);
         }
  }
  showToast(messagecode: string) {
    this.toastController.create({
      message: messagecode,
      duration: 2000,
      animated: true,
      showCloseButton: true,
      closeButtonText: 'OK',
      position: 'middle'
    }).then((obj) => {
      obj.present();
    });
  }
}
