import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { WelcomePage } from './welcome/welcome.page';
import { HomePage } from './home/home.page';
import { WelcomePageModule } from './welcome/welcome.module';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'reg', component: RegisterComponent },
  { path: 'home', component: HomePage },

];

@NgModule({
  imports: [
   RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
